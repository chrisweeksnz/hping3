FROM alpine:latest
LABEL maintainer="Chris Weeks <chris@weeks.net.nz>" \
      project-site="https://tools.kali.org/information-gathering/hping3" \
      container-site="https://gitlab.com/chrisweeksnz/hping3"
RUN echo 'http://dl-cdn.alpinelinux.org/alpine/edge/testing' >> /etc/apk/repositories && \
    apk update && \
    apk add hping3
ENTRYPOINT ["/usr/sbin/hping3"]
